package com.dwtraining.databinding

import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.BindingAdapter
import androidx.lifecycle.LiveData
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.dwtraining.adapters.MoviesAdapter
import com.dwtraining.models.Movie

/**
 * @author Giovani González
 * Created by Giovani on 2019-06-23.
 */

@BindingAdapter("android:movieItems")
fun setItems(recycler: RecyclerView, items: LiveData<List<Movie>>) {
    recycler.adapter?.let { adapter ->
        if (adapter is MoviesAdapter) {
            items.observe(recycler.context as AppCompatActivity) {
                adapter.refreshDataMovies(it)
            }
        }
    }
}

@BindingAdapter("android:setAdapter")
fun setAdapter(recycler: RecyclerView, adapter: MoviesAdapter?) {
    adapter?.let {
        recycler.adapter = it
        recycler.layoutManager = GridLayoutManager(recycler.context as AppCompatActivity, 2)
    }
}