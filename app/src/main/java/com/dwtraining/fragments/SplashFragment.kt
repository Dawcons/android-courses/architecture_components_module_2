package com.dwtraining.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.dwtraining.archcompmodule2.R
import com.dwtraining.base.BaseFragment

/**
 * @author Giovani González
 * Created by Giovani on 2019-06-29.
 */
class SplashFragment : BaseFragment() {

    private val TAG = SplashFragment::class.java.simpleName

    override fun setContentView(container: ViewGroup?): View {
        return LayoutInflater.from(context).inflate(R.layout.fragment_splash, container, false)
    }

    override fun onViewFragmentCreated(view: View, savedInstanceState: Bundle?) {
        fullScreen()
        startTimer()
    }

    private fun startTimer() {
        object: Thread() {
            override fun run() {
                try {
                    sleep(2500)
                } catch (e: Exception) {
                    Log.e(TAG, "Something went wrong.")
                } finally {
                    view?.post { findNavController().navigate(R.id.loginFragment) }
                }
            }
        }.start()
    }
}