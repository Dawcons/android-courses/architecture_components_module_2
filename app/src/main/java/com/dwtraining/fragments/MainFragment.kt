package com.dwtraining.fragments

import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import com.dwtraining.adapters.MoviesAdapter
import com.dwtraining.archcompmodule2.R
import com.dwtraining.archcompmodule2.databinding.FragmentMainBinding
import com.dwtraining.base.BaseFragment
import com.dwtraining.viewmodels.MoviesViewModel

/**
 * @author Giovani González
 * Created by Giovani on 2019-06-22.
 */
class MainFragment : BaseFragment() {

    private val moviesViewModel: MoviesViewModel by viewModels()
    private lateinit var binding: FragmentMainBinding

    override fun setContentView(container: ViewGroup?): View =
        FragmentMainBinding.inflate(layoutInflater, container, false).apply {
            viewmodel = moviesViewModel.apply {
                isRefreshing.observe(viewLifecycleOwner) {
                    swipeRefreshMovies.isRefreshing = it
                }
            }
            adapter = MoviesAdapter()
            lifecycleOwner = viewLifecycleOwner
            binding = this
        }.root

    override fun onViewFragmentCreated(view: View, savedInstanceState: Bundle?) {
        showBackIconOnToolbar()
        setTitle(getString(R.string.movies_list))
        binding.swipeRefreshMovies.setOnRefreshListener {
            if (binding.swipeRefreshMovies.isRefreshing) {
                moviesViewModel.getMovies(true)
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }
}